import AsyncStorage from '@react-native-community/async-storage';
export default class LocalMongo {
    makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

    async saveDocument(toSave){
        try {
            const uid = this.makeid(32);
            await AsyncStorage.setItem(uid, JSON.stringify(toSave));
            const doc = await AsyncStorage.getItem(uid);
            return {...JSON.parse(doc),...{id: uid}}
        }
        catch(exception) {
            return false;
        }
    }
    async readAll(){
        const doc = await AsyncStorage.getItem('theme');
        await AsyncStorage.removeItem('theme');

        const keys = await AsyncStorage.getAllKeys();
        const result = await AsyncStorage.multiGet(keys);
        
        const toReturn = result.map(req =>{
            return {
                ...JSON.parse(req[1]), 
                ...{id: req[0]}
            }
        })

        AsyncStorage.setItem('theme', String(doc));

        return toReturn.sort((a, b) => {
            const adateSplits = a.date.split('/')
            const aDate = new Date(`${adateSplits[1]}/${adateSplits[0]}/${adateSplits[2]} 08:00`)

            const bdateSplits = b.date.split('/')
            const bDate = new Date(`${bdateSplits[1]}/${bdateSplits[0]}/${bdateSplits[2]} 08:00`)

            if(aDate > bDate) return 1;
            else return -1;
        })
    }
    async deleteByID(idToDelete){
        try {
            await AsyncStorage.removeItem(idToDelete);
            return true;
        }
        catch(exception) {
            return false;
        }
    }
    async updateStatus(idToUpdate){
        const jsonString = await AsyncStorage.getItem(idToUpdate)
        const item = JSON.parse(jsonString)
        const todo = {
            text: item.text,
            date: item.date,
            push: item.push,
            status: 1,
            creationDate: item.creationDate,
          }
        await this.deleteByID(idToUpdate)
        const toReturn = await this.saveDocument(todo)
        return toReturn
    }

    async saveTheme(themeId){
        try {
            await AsyncStorage.removeItem('theme');
            await AsyncStorage.setItem('theme', String(themeId));
        }
        catch(exception) {
            return false;
        }
    }

    async getTheme(){
        try {
            const doc = await AsyncStorage.getItem('theme');
            return Number(doc)
        }
        catch(exception) {
            return Number(2);
        }
    }

}