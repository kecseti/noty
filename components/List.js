import React from 'react';
import { StyleSheet, Text, View, ScrollView, SafeAreaView} from 'react-native';
import { NeuView, NeuSpinner } from 'react-native-neu-element';
import { Icon } from 'react-native-elements'
import TodoItem from './Todo'

export default class TodoList extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      list : []
    }
  }

  async componentDidMount() {
    const list = await this.props.db.readAll();
    this.setState({ list }) 
  }

  render() {
      const mainColor = this.props.color
      const width = this.props.width
      const height = this.props.height
      if(!this.state.list.length){
        return (
          <NeuView color={ mainColor } height={height-320} width={width-80} borderRadius={16} style={{display:'flex',marginTop:30, marginBottom: 20}} inset>
            <SafeAreaView style={{width:width, display:'flex', alignItems:'center'}}>
                <ScrollView style={{width:width-150,}} contentContainerStyle={{alignItems:'center'}}>
                  <Text style= {{ textAlign:'center', color: mainColor, textShadowColor: 'rgba(0, 0, 0, 0.15)', textShadowOffset: {width: +2, height: +2}, textShadowRadius: 4, fontSize:45,  fontWeight:'bold'}}>You don't have anything to do!</Text>
                </ScrollView>
            </SafeAreaView>
          </NeuView>
        )
      }
      return(
        <NeuView color={ mainColor } height={height-320} width={width-80} borderRadius={16} style={{display:'flex',marginTop:30, marginBottom: 20}} inset>
          <SafeAreaView style={{width:width, display:'flex', alignItems:'center'}}>
              <ScrollView style={{width:width,}} contentContainerStyle={{alignItems:'center'}}>
                {
                  this.state.list.map( 
                    document => 
                    <TodoItem db = {this.props.db} key = {document.id} color= {mainColor} width={width-120} height={100} text={document.text} date={document.date} push={document.push} id = {document.id} status = {document.status} />

                  )
                }
              </ScrollView>
          </SafeAreaView>
        </NeuView>
        
    );
  }
}
