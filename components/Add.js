import React from 'react';
import { Text, TouchableOpacity, ScrollView, SafeAreaView} from 'react-native';
import { NeuView, NeuInput } from 'react-native-neu-element';
import TodoItem from './Todo'
import DatePicker from 'react-native-datepicker';
import moment from 'moment'
import { Snackbar } from 'react-native-paper';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { Divider } from 'react-native-elements';

export default class TodoList extends React.Component {
  constructor(props){
    super(props)
    this.state = {
        text : '',
        date: moment().format("DD/MM/YYYY"),
        isEnabled: false,
        switch:'OFF',
        snackbar: false,
        snackText:'',
        error: false
    }
  }
  
  askNotification = async () => {
    // We need to ask for Notification permissions for ios devices
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    if (Constants.isDevice && status === 'granted')
      console.log('Notification permissions granted.');
  };

  componentDidMount(){
    this.askNotification();
  }

  askPermissions = async () => {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      return false;
    }
    return true;
  };

  
  setText = (event) => {
    this.setState({
        text: event
    })
  }

  setDate = (event) => {
    this.setState({
        date: event
    })
  }

  saveTodo = async  () => {
    if(this.state.text !== ''){
      const todo = {
        text: this.state.text,
        date: this.state.date,
        push: this.state.isEnabled,
        status: 0,
        creationDate: new Date()
      }
      const answer = await this.props.db.saveDocument(todo)
      if(answer === false){
        this.setState({
          snackbar: true, 
          snackText: 'Addig TODO failed!',
          error: true,
        })
      } else{
        if(this.state.isEnabled){
          this.askPermissions()
          const dateSplits = this.state.date.split('/')
          Notifications.scheduleLocalNotificationAsync(
            {
              title: "You've got a thing TODO!",
              body: this.state.text,
            },
            {
              time: new Date(`${dateSplits[1]}/${dateSplits[0]}/${dateSplits[2]} 08:00`).getTime(),
            },
          );
        }

        this.setState({
          text : '',
          date: moment().format("DD/MM/YYYY"),
          isEnabled: false,
          switch:'OFF',
          snackbar: true, 
          snackText: 'TODO added!',
          error: false,
        })
      }
     
    } else {
      this.setState({
        snackbar: true, 
        snackText: 'Can not add empty TODO!',
        error: true,
      })
    }
  }

  render() {
      const mainColor = this.props.color
      const width = this.props.width
      const height = this.props.height
      return(
        <NeuView color={ mainColor } height={height-320} width={width-80} borderRadius={16} style={{marginTop:30, marginBottom: 20}} containerStyle = {{justifyContent:'flex-start',paddingTop: 0}} inset>
           <SafeAreaView style={{width:width-80,height:height-320, display:'flex', alignItems:'center'}}>
              <ScrollView style={{width:width-80,}} contentContainerStyle={{alignItems:'center'}}>
                <TodoItem color= {mainColor} width={width-120} height={100} text={this.state.text} date={this.state.date} push={this.state.isEnabled} disabled={true} status = {0}></TodoItem>
                <NeuView color={ mainColor } height={2} width={width-100} inset></NeuView>

                <Text style={{alignSelf:'flex-start',fontWeight:'bold', fontSize:15,marginLeft:20, marginBottom: 5, marginTop:10}}>You need to do:</Text>
                <NeuInput color={ mainColor } height={height/20} width={width-120} borderRadius={16} onChangeText={this.setText} value={this.state.text} placeholder="Ex. Take a nap!"></NeuInput>
                <Text style={{alignSelf:'flex-start',fontWeight:'bold', fontSize:15,marginLeft:20, marginTop:15, marginBottom: 5}}>Due date</Text>
                <NeuView color={ mainColor } height={height/20} width={width-120} inset borderRadius={16}>
                    <DatePicker showIcon={false} androidMode="spinner" style={{ width: width - 120 }} date={this.state.date} mode="date" placeholder="DD/MM/YYYY" format="DD/MM/YYYY"
                        customStyles={{
                        dateInput: {
                            backgroundColor: mainColor,
                            borderRadius:16,
                            borderColor:'transparent',
                            alignItems:'flex-start',
                            paddingLeft:15,

                            },
                        }}
                        onDateChange={(date) => { this.setState({ date: date }); }}
                        />
                </NeuView>   
                <Text style={{alignSelf:'flex-start',fontWeight:'bold', fontSize:15,marginLeft:20, marginTop:20, marginBottom: 5}}>Push notification on due day</Text>   
                <TouchableOpacity onPress={() => {this.setState({ isEnabled: !this.state.isEnabled, switch: this.state.switch === 'OFF' ? 'ON' : 'OFF'  }); }}>
                  <NeuView color={ mainColor } height={height/20} width={width-120} inset borderRadius={16}>
                    <NeuView color= {mainColor} width={height/25} height={height/25} borderRadius={15} style={{position:'absolute', left: !this.state.isEnabled ? 10 : width - 160}}></NeuView>
                      <Text style= {{ position:'absolute', color: mainColor, textShadowColor: 'rgba(255, 255, 255, 0.05)', textShadowOffset: {width: -2, height: -2}, textShadowRadius: 4, fontSize:height/20,  fontWeight:'bold'}}>{this.state.switch}</Text>
                      <Text style= {{ color: mainColor, textShadowColor: 'rgba(0, 0, 0, 0.15)', textShadowOffset: {width: +2, height: +2}, textShadowRadius: 4, fontSize:height/20,  fontWeight:'bold'}}>{this.state.switch}</Text>
                  </NeuView>
                </TouchableOpacity>
            </ScrollView>
          </SafeAreaView>
          
          <TouchableOpacity style={{position:'absolute', bottom:10}}onPress={this.saveTodo}>
            <NeuView color={ mainColor } height={height/20} width={width-120}  borderRadius={16}>
              <Text style={{fontWeight:'bold', fontSize:height/36, }}>Add TODO</Text>   
            </NeuView>
          </TouchableOpacity>

            <Snackbar visible={this.state.snackbar} onDismiss={() => {this.setState({snackbar: false})}} duration={4000}  style={{backgroundColor:mainColor, borderColor:!this.state.error ? 'rgba(0,125,0,0.3)' : 'rgba(125,0,0,0.3)' , borderWidth: 4}}>
              <Text style={{fontWeight:'bold', fontSize:15, color:'black'}}>{this.state.snackText}</Text>   
            </Snackbar>
        </NeuView>
        
    );
  }
}
