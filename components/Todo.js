import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { NeuView } from 'react-native-neu-element';
import { Icon } from 'react-native-elements'
import { Badge } from 'react-native-elements'

export default class TodoItem extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      status: this.props.status,
      id : this.props.id,
      snackbar: true,
      snackText:'',
      error: false
    }
  }

  setDone = async () =>{
    if(this.state.status === 0){
     const answer = await this.props.db.updateStatus(this.props.id)
      this.setState({
        status: 1,
        id: answer.id
      })
    }
    else {
      const answer = await this.props.db.deleteByID(this.state.id)
      this.setState({
        status: 2
      })
    }
  }

  render() {
      if(this.state.status === 2) return null;
      return(
        <NeuView color= {this.props.color} width={this.props.width} height={this.props.height} style={{marginBottom:15, marginTop:15}}>
            <View style={{width:this.props.width - 60, position:'absolute', top:20, left: 0}}>
                <Text style={{position:'absolute', left: 10, fontWeight:'bold', fontSize:20}}>{this.props.text}</Text>
            </View>
            <Text style={{position:'absolute', top: 5, left: 10, fontWeight:'bold', fontSize:12}}>{this.props.date}</Text>
            <View style={{position:'absolute', right: 10}}>
                <TouchableOpacity onPress={this.setDone} disabled = {this.props.disabled}>
                  <NeuView color= {this.props.color} width={45} height={45} borderRadius={10}>
                      <Icon name={this.state.status === 0 ? 'check' : 'delete-outline'} color = {this.state.status === 0 ? 'rgba(0,125,0,0.7)' : 'rgba(125,0,0,0.3)'} size={40}/>
                  </NeuView>
                </TouchableOpacity>
            </View>
            {
              this.props.push && this.state.status === 0 ? 
              <Badge value={<Icon name="notifications-none" />} status="error"  containerStyle={{ position: 'absolute', top: -10, right: -10}} badgeStyle={{ backgroundColor:this.props.color, borderColor:'transparent', borderRadius:0, height:30 }}/>
              :
              this.state.status !== 0 ?
              <Badge value={<Icon name="done-all" />} status="error"  containerStyle={{ position: 'absolute', top: -10, right: -10}} badgeStyle={{ backgroundColor:this.props.color, borderColor:'transparent', borderRadius:15, height:30}}/>
              :
              null
            }
        </NeuView>
        
    );
  }
}
