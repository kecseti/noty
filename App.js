import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text, View, TouchableOpacity} from 'react-native';
import { NeuView } from 'react-native-neu-element';
import { Dimensions } from 'react-native';
import { colors, Icon } from 'react-native-elements'
import TodoList from './components/List'
import AddItem from './components/Add'
import LocalMongo from './components/datastorage'

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      colors: ['#b88e8d', '#aa8976', '#D2DBEE', '#4e8098', '#7b506f','#949cdf', '#9CC5A1'],
      colorIndex : 2,
      listMode: true
    }
  }

  nextColor = async () =>{
    const localMongo = new LocalMongo()
    await localMongo.saveTheme(this.state.colorIndex == this.state.colors.length - 1 ? 0 : this.state.colorIndex + 1)
    this.setState({ colorIndex: this.state.colorIndex == this.state.colors.length - 1 ? 0 : this.state.colorIndex + 1 })
    
  }

  prevColor = async () =>{
    const localMongo = new LocalMongo()
    await localMongo.saveTheme(this.state.colorIndex == 0 ? this.state.colors.length - 1 : this.state.colorIndex - 1)
    this.setState({ colorIndex: this.state.colorIndex == 0 ? this.state.colors.length - 1 : this.state.colorIndex - 1 })
   
  }

  async componentDidMount(){
    const localMongo = new LocalMongo()
    const index = await localMongo.getTheme()
    this.setState({ colorIndex: index })
  }

  render() {
    const localMongo = new LocalMongo()
    const mainColor = this.state.colors[this.state.colorIndex];
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    return (
      <View style={{flex: 1, backgroundColor: mainColor, alignItems: 'center'}}>
        <Text style= {{ position:'absolute', color: mainColor, textShadowColor: 'rgba(255, 255, 255, 0.05)', textShadowOffset: {width: -4, height: -4}, textShadowRadius: 8, fontSize:150,  fontWeight:'bold'}}>Noty</Text>
        <Text style= {{ color: mainColor, textShadowColor: 'rgba(0, 0, 0, 0.15)', textShadowOffset: {width: +4, height: +4}, textShadowRadius: 8, fontSize:150,  fontWeight:'bold'}}>Noty</Text>

        {
          this.state.listMode ? 
          <TodoList color = {mainColor} width = { width } height = { height } db = {localMongo}></TodoList>
          :
          <AddItem color = {mainColor} width = { width } height = { height } db = {localMongo}></AddItem>
        }
        <View style={{flex:1, justifyContent:'space-between', flexDirection:'row', width: width, alignItems:'center'}}>
        <TouchableOpacity onPress={this.prevColor} style={{margin: 10}}>
            <NeuView color= {mainColor} width={60} height={60} borderRadius={30}>
              <Icon name='chevron-left' color = {'rgba(0,0,0,0.5)'} size={ 40 }  style={{padding:0, margin:0}}/>
            </NeuView>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {this.setState({ listMode: !this.state.listMode }); }}>
            <NeuView color= {mainColor} width={100} height={100} borderRadius={50}>
              <Icon name={this.state.listMode ? 'add' : 'clipboard' } type={this.state.listMode ? '' : 'feather' }  color = {'rgba(0,0,0,0.5)'} size={this.state.listMode ? 100 : 70 }  style={{padding:0, margin:0}}/>
            </NeuView>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.nextColor} style={{margin: 10}}>
            <NeuView color= {mainColor} width={60} height={60} borderRadius={30}>
              <Icon name='chevron-right' color = {'rgba(0,0,0,0.5)'} size={ 40 }  style={{padding:0, margin:0}}/>
            </NeuView>
          </TouchableOpacity>

        </View>
        <StatusBar translucent={true} backgroundColor="transparent" />
      </View>
    );
  }
  
}
